import Component from '@ember/component';
import { computed } from '@ember/object';


export default Component.extend({
  init(){
    this._super(...arguments);
    this.set("messageApproved", false);
  },
  willDestroyElement() {
    this._super(...arguments);

    let dialogId = this.getWithDefault('confirmDialogId', 'confirmDialog');
    $('#'+dialogId).modal('hide');
    this.set("messageApproved", false);
  },
  didDestroyElement() {
    this._super(...arguments);

    document.body.className = 'ember-application';
    document.body.style.cssText = '';
  },
  hasTitle: computed('titleText', 'titleIconClass', function () {
    return this.get("titleText") || this.get("titleIconClass")
  }),
  titleSymbolColor: computed('titleIconColor', function () {
    return this.getWithDefault("titleIconColor", "black")
  }),
  titleMessageColor: computed('titleTextColor', function () {
    return this.getWithDefault("titleTextColor", "black")
  }),
  hasHeader: computed('headerText', 'headerIconClass', function () {
    return this.get("headerText") || this.get("headerIconClass")
  }),
  headerSymbolColor: computed('headerIconColor', function () {
    return this.getWithDefault("headerIconColor", "black")
  }),
  headerMessageColor: computed('headerTextColor', function () {
    return this.getWithDefault("headerTextColor", "black")
  }),
  hasCheckbox: computed('showCheckbox', 'checkboxMessage', function () {
    return this.get("showCheckbox") && this.get("checkboxMessage")
  }),
  disablePrimaryButton: computed('hasCheckbox', 'messageApproved', function () {
    return this.get("hasCheckbox") && !this.get("messageApproved");
  }),
  confirmBtnText: computed('confirmText', function () {
    return this.getWithDefault("confirmText", "Confirm");
  }),
  cancelBtnText: computed('cancelText', function () {
    return this.getWithDefault("cancelText", "Cancel");
  }),
  actions: {
    confirm() {
      let dialogId = this.getWithDefault('confirmDialogId', 'confirmDialog');
      console.log(dialogId);
      $('#'+dialogId).modal('hide');
      this.set("messageApproved", false);
      this.sendAction();
    },
    cancel() {
      let dialogId = this.getWithDefault('confirmDialogId', 'confirmDialog');
      var cancelAction = this.get("cancelAction");
      if (this.cancelAction){
        this.cancelAction();
      }
      $('#'+dialogId).modal('hide');
      this.set("messageApproved", false);
    },
  }
});
